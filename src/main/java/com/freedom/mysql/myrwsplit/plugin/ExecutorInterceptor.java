package com.freedom.mysql.myrwsplit.plugin;

import java.util.Properties;

import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.BaseExecutor;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.transaction.jdbc.JdbcTransaction;

import com.freedom.mysql.myrwsplit.helper.LoggerHelper;
import com.freedom.mysql.myrwsplit.helper.ThreadLocalHelper;

import org.apache.ibatis.plugin.Intercepts;

/**
 * 数据库操作性能拦截器,记录耗时
 * 
 * @Intercepts定义Signature数组,因此可以拦截多个,但是只能拦截类型为： Executor ParameterHandler
 *                                              StatementHandler
 *                                              ResultSetHandler
 */
// 拦截BaseExecutor,这样，可以拦截各种类型的Executor实现
@Intercepts(value = {
		@Signature(type = Executor.class, method = "query", args = { MappedStatement.class, Object.class,
				RowBounds.class, ResultHandler.class }),
		@Signature(type = Executor.class, method = "query", args = { MappedStatement.class, Object.class,
				RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class }),
		@Signature(type = Executor.class, method = "update", args = { MappedStatement.class, Object.class })

})
public class ExecutorInterceptor implements Interceptor {
	private static LoggerHelper LOGGER = LoggerHelper.getLogger(ExecutorInterceptor.class);

	public ExecutorInterceptor() {
	}

	// 在getConnection之前，必须保存sql类型,作为获取物理连接的理论依据
	@SuppressWarnings("unused")
	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		// 0)reset all
		ThreadLocalHelper.reset();
		// 1)get AutoCommit type
		BaseExecutor baseExecutor = (BaseExecutor) invocation.getTarget();// 比如是SimpleExecutor
		JdbcTransaction jdbcTransaction = (JdbcTransaction) baseExecutor.getTransaction();
		MetaObject metaObject = SystemMetaObject.forObject(jdbcTransaction);
		Object autoCommitObject = metaObject.getValue("autoCommmit");// 第1行完全是原作者笔误
		if (null == autoCommitObject) {
			autoCommitObject = metaObject.getValue("autoCommit");
		}
		LOGGER.info("autoCommit ---> " + (boolean) autoCommitObject);
		ThreadLocalHelper.AutoCommitThreadLocal.set((boolean) autoCommitObject);
		// 2)get sql type
		Object[] args = invocation.getArgs();
		MappedStatement ms = (MappedStatement) args[0];
		SqlCommandType sqlType = ms.getSqlCommandType();
		ThreadLocalHelper.SqlCommandTypeThreadLocal.set(sqlType);// save it to
																	// local
																	// thread
		LOGGER.info("set ThreadLocal sqlCommandType ---> " + sqlType);
		// 3)continue to work
		long start = System.currentTimeMillis();
		Object result = invocation.proceed();
		long end = System.currentTimeMillis();
		return result;
	}

	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {

	}

}