package com.freedom.mysql.myrwsplit.datasource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import javax.sql.DataSource;

import org.apache.ibatis.mapping.SqlCommandType;

import com.alibaba.druid.pool.DruidDataSource;
import com.freedom.mysql.myrwsplit.helper.LoggerHelper;
import com.freedom.mysql.myrwsplit.helper.ThreadLocalHelper;
import com.freedom.mysql.myrwsplit.helper.UrlHelper;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.alibaba.druid.pool.DruidPooledConnection;

@SuppressWarnings("serial")
public class MasterSlaveDataSource extends DruidDataSource {
	private static LoggerHelper LOGGER = LoggerHelper.getLogger(MasterSlaveDataSource.class);
	private static AtomicInteger COUNTER = new AtomicInteger(0);
	private DataSource masterDataSource = null;
	private DataSource[] slaveDataSources = null;

	public MasterSlaveDataSource(Properties properties) {
		try {
			ArrayList<String> urls = UrlHelper.handle(properties.getProperty("url"));
			slaveDataSources = new DataSource[urls.size() - 1];
			int index = 0;
			for (String url : urls) {
				properties.setProperty("url", url);
				if (0 == index) {
					properties.put("defaultReadOnly", "false");
					masterDataSource = DruidDataSourceFactory.createDataSource(properties);
				} else {
					properties.put("defaultReadOnly", "true");
					slaveDataSources[index - 1] = DruidDataSourceFactory.createDataSource(properties);
				}
				index++;
			}
		} catch (Exception e) {
			LOGGER.error(e.toString());
			System.exit(-1);
		}

	}

	@Override
	public DruidPooledConnection getConnection() throws SQLException {
		// 1)get 还要考虑是否自动提交,也就是是否开启了事务
		boolean autoCommit = ThreadLocalHelper.AutoCommitThreadLocal.get();
		String sql = ThreadLocalHelper.BoundSqlThreadLocal.get();
		SqlCommandType type = ThreadLocalHelper.SqlCommandTypeThreadLocal.get();
		// 2)use them
		if (false == autoCommit) {
			// 开启事务--->无条件走主库
			LOGGER.info("get Connection by MasterSlaveDataSource--->use masterDataSource");
			return (DruidPooledConnection) masterDataSource.getConnection();
		} else {
			// 没有开启事务-自动提交的
			if (SqlCommandType.SELECT != type) {
				// 没有开启事务-增删改
				LOGGER.info("get Connection by MasterSlaveDataSource--->use masterDataSource");
				return (DruidPooledConnection) masterDataSource.getConnection();
			} else {
				// 查询也分是否强制走主库
				if (-1 != sql.indexOf("/*FORCE_MASTER*/")) {
					LOGGER.info("get Connection by MasterSlaveDataSource--->use masterDataSource");
					return (DruidPooledConnection) masterDataSource.getConnection();
				} else {
					LOGGER.info("get Connection by MasterSlaveDataSource--->use slaveDataSources");
					// retry算法
					int slaveIndex = COUNTER.getAndIncrement();
					if (slaveIndex < 0) {
						slaveIndex = 0;
					}
					// 归一
					slaveIndex = slaveIndex % slaveDataSources.length;
					// ok,let us fetch the connection
					for (int i = 0; i < slaveDataSources.length; i++) {
						DruidPooledConnection connection = (DruidPooledConnection) slaveDataSources[(slaveIndex++)
								% slaveDataSources.length].getConnection();
						if (null != connection) {
							return connection;
						}
					}
					throw new SQLException(
							"fail to get slave mysql connection,maybe all slave machines DOWN,check it now!");
				}
			}
		}

	}

	// private static int random(int min, int max) {
	// Random random = new Random();
	// int selectedIndex = random.nextInt(max) % (max - min + 1) + min;
	// LOGGER.info("" + selectedIndex);
	// return selectedIndex;
	// }

	public static void main(String[] args) {
		for (int index = 0; index < 100; index++) {
			// LOGGER.info("" + random(0, 2));
		}
	}
}
